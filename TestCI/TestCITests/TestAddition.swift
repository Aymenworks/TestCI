//
//  TestAddition.swift
//  TestCI
//
//  Created by Rebouh Aymen on 17/10/2016.
//  Copyright © 2016 Worldline. All rights reserved.
//

import XCTest

class TestAddition: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSuccess() {
        XCTAssertEqual(2+2, 4)
    }
    
    func testWithMistakes() {
        XCTAssertEqual(2+2, 5)
    }
    
    func testWithOthersMistakes() {
        XCTAssertEqual(1+3, 5)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
